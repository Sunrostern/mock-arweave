FROM node:16

WORKDIR /app

COPY package.json ./
COPY yarn.lock ./
RUN yarn

COPY ./ ./
ENV PORT=1984
EXPOSE 1984
CMD ["node", "src/index.js"]