'use strict';

const app = require('express')();

const port = process.env.PORT || 1984;

app.get('/', (request, response) => {
  response.send('Arweave');
});

app.listen(port, () => {
  console.log(`App listening at Port ${port}.`);
});